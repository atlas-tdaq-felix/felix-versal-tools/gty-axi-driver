/*
 * file: gkos_char_device.c
 *
 * Desc: A simple device that
 *      echos a message when read,
 *      write method not implemented
 *
 *      This was made on top of
 *      LDD and LKMPG examples
 *      https://gist.github.com/dhilst/5479135
 *
 *
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/device.h>
#include <linux/io.h>
#include <linux/types.h>
#include <linux/delay.h>

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include <linux/ioctl.h>

#define FLX182_APB_ADDR_BASE 0x20180000000
#define FLX182_APB_ADDRHIGHADDR 0x2018000FFFF

#define FLX182_LTI_GPIO_ADDR_BASE 0x20100020000
#define FLX182_LTI_GPIO_ADDRHIGHADDR 0x2010002FFFF

#define DEVICE_NAME "AXI_GTY"
/*
 * Prototypes
 */
// static int init_module(void);
// void cleanup_module(void);
static int dev_open(struct inode *inode, struct file *);
static int dev_release(struct inode *inode, struct file *);
static ssize_t dev_read(struct file *fp, char *buf, size_t len, loff_t *off);
static ssize_t dev_write(struct file *, const char *buf, size_t len, loff_t *off);
static int dev_init(void);
static struct class *device_class;
static struct device *device_struct;
static long device_file_ioctl(struct file *file_ptr, u_int cmd, u_long arg);
static void __iomem *reg_ptr = NULL;
static void __iomem *lti_gpio_reg_ptr = NULL;
static int device_file_major_number = 0;
static const char device_name[] = DEVICE_NAME;
static int device_number = 0;


/*
 * Our variables, @use_counter
 * will block concurrenty opens.
 * @buffer is the message and
 * @buffer_len the lenght of @buffer (duh)
 */
static char use_counter = 0;

static dev_t dev;
static struct cdev *cdevp;

static struct file_operations fops = {
    .owner = THIS_MODULE,
    .read = dev_read,
    .write = dev_write,
    .open = dev_open,
    .unlocked_ioctl = device_file_ioctl,
    .release = dev_release};

/*
 * Any device specific initialization
 * goes here. Its called at bottom of init_module()
 */
static int dev_init(void)
{
    reg_ptr = ioremap(FLX182_APB_ADDR_BASE, 0x10000);
    lti_gpio_reg_ptr = ioremap(FLX182_LTI_GPIO_ADDR_BASE, 0x10000);

    printk("Init reg_ptr %x\n", reg_ptr);
    printk("Init lti_gpio_reg_ptr %x\n", lti_gpio_reg_ptr);
    return 0;
}

/*
 * Called when device is opened
 */
static int dev_open(struct inode *inode, struct file *fp)
{
    if (use_counter)
        return -EBUSY;
    use_counter++;
    try_module_get(THIS_MODULE);

    return 0;
}

/*
 * Called when device is released. The device is
 * released when there is no process using it.
 */
static int dev_release(struct inode *inode, struct file *fp)
{
    use_counter--;
    module_put(THIS_MODULE);

    return 0;
}

/*
 * @off controls
 * the "walk" through our buffer, is whith @off
 * that we say to user where is stoped.
 * @len is how much bytes to read. I almost ignore it.
 * I just check if is greater than 0.
 *
 * Called when device is read.
 * This method will read one, and only one byte per call,
 * If @off is longer than my buffer size or len is not
 * greater than 0 it returns 0, otherwise I copy one byte
 * to user buffer and returns the bytes readed, so 1.
 */
static ssize_t dev_read(struct file *fp, char *buf, size_t len, loff_t *off)
{
    // if (*off >= buffer_len || len <= 0)
    //     return 0;

    // if (copy_to_user(buf, &buffer[*off], 1u))
    //     return -EFAULT;

    // (*off)++;
    return 1;
}

#define GTY_READ 0X01
#define GTY_WRITE 0X03
#define LTI_GPIO_R 0x05
#define LTI_GPIO_W 0x07

typedef struct _reg_write_arg
{
    uint32_t addr;
    uint32_t val;
} reg_write_arg_t;

static long device_file_ioctl(struct file *file_ptr, u_int cmd, u_long arg)
{

    uint32_t temp;
    uint32_t *reg_addr;
    reg_write_arg_t reg_write_arg;
    // printk("IOCTL. CMD %x", cmd);

    //    printk(KERN_NOTICE "AXI-lite_driver: doing cmd: %u, setting parameter to : 0x%lX\n", cmd, arg);

    if (copy_from_user((void *)&reg_write_arg, (void *)arg, sizeof(reg_write_arg_t)) != 0)
    {
        ////printk(("flx(flx_ioctl, GETLOCK) error from copy_from_user\n"));
        return (-EFAULT);
    }
//    if(reg_write_arg.addr > 0xFFFF){
//        printk("Illegal addr:%04x\n",reg_write_arg.addr);
//        return (-EFAULT);
//    }
    // printk("Copy ok");

    if (cmd == GTY_READ)
    {
        // printk("Read: %x",reg_write_arg.addr);

        reg_write_arg.val = readl(reg_ptr + reg_write_arg.addr);
        // printk("returning value %08x\n", temp);

        if (copy_to_user(((u_int *)arg), &reg_write_arg, sizeof(reg_write_arg_t)) != 0)
        {
            // printk(("flx(flx_ioctl, GETLOCK) Copy card_params_t to user space failed!\n"));
            return (-EFAULT);
        }
    }
    else if (cmd == GTY_WRITE)
    {
        // printk("Write: %04x, %08X", reg_write_arg.addr, reg_write_arg.val);
        writel(reg_write_arg.val, reg_ptr + reg_write_arg.addr);
    }
    else if (cmd == LTI_GPIO_R)
    {
        reg_write_arg.val = readl(lti_gpio_reg_ptr + reg_write_arg.addr);
        // printk("returning value %08x\n", temp);

        if (copy_to_user(((u_int *)arg), &reg_write_arg, sizeof(reg_write_arg_t)) != 0)
        {
            // printk(("flx(flx_ioctl, GETLOCK) Copy card_params_t to user space failed!\n"));
            return (-EFAULT);
        }
    }
    else if (cmd == LTI_GPIO_W)
    {
        writel(reg_write_arg.val, lti_gpio_reg_ptr + reg_write_arg.addr);
    }

    // vfree(reg_ptr);

    return 0;
}

static ssize_t dev_write(struct file *fp, const char *buf, size_t len,
                         loff_t *off)
{
    // void *reg_ptr = ioremap(FLX182_APB_ADDR_BASE, 0x1000);
    // if (copy_from_user(reg_ptr + *off, buf, len))
    //     return -EFAULT;
    // //*(uint32_t *)(reg_ptr+off)= *((uint32_t*)buf);
    return len;
}

/*
 * Called when module is load
 */
static int my_init_module(void)
{
    int error;
    int result = 0;
    int ret;
    /* Alloc a device region */
    error = alloc_chrdev_region(&dev, 1, 1, DEVICE_NAME);
    if (error)
        goto error_out;

    /* Registring */
    cdevp = cdev_alloc();
    if (!cdevp)
        return -ENOMEM;

    /* Init it! */
    cdev_init(cdevp, &fops);

    /* Tell the kernel "hey, I'm exist" */
    error = cdev_add(cdevp, dev, 1);
    if (error < 0)
        goto error_out;

    printk(KERN_INFO DEVICE_NAME " registred with major %d\n", MAJOR(dev));
    printk(KERN_INFO DEVICE_NAME " do: `mknod /dev/%s c %d %d' to create "
                                 "the device file\n",
           DEVICE_NAME, MAJOR(dev), MINOR(dev));

    /* Device initialization isn't needed yet */
    if (dev_init())
        goto error_out;
    result = register_chrdev( 0, device_name, &fops );
    if( result < 0 )
    {
        printk( KERN_WARNING DEVICE_NAME ":  can\'t register character device with errorcode = %i\n", result );
        return result;
    }
    device_file_major_number = result;
    printk( KERN_NOTICE DEVICE_NAME ": registered character device with major number = %i and minor numbers 0...255\n", device_file_major_number );

    device_class = class_create(device_name );
    if( IS_ERR( device_class ) )
    {
        printk( KERN_WARNING DEVICE_NAME":  can\'t create device class with errorcode = %li\n", PTR_ERR( device_class ) );
        unregister_chrdev( device_file_major_number, device_name );
        return PTR_ERR( device_class );
    }
    printk( KERN_NOTICE DEVICE_NAME": Device class created\n" );

    device_number = MKDEV( device_file_major_number, 0 );
    device_struct = device_create( device_class, NULL, device_number, NULL, device_name );
    

    return 0;

error_out:
    return -EFAULT;
}

void my_cleanup_module(void)
{
    iounmap(reg_ptr);
    cdev_del(cdevp);
}

module_init(my_init_module);
module_exit(my_cleanup_module);

MODULE_LICENSE("GPL");
